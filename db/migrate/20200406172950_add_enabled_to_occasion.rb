# frozen_string_literal: true

class AddEnabledToOccasion < ActiveRecord::Migration[6.0]
  def change
    add_column :occasions, :enabled, :boolean
  end
end
