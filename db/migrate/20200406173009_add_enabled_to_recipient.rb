# frozen_string_literal: true

class AddEnabledToRecipient < ActiveRecord::Migration[6.0]
  def change
    add_column :recipients, :enabled, :boolean
  end
end
