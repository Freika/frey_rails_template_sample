# frozen_string_literal: true

class ChangeOccasionNameToTitle < ActiveRecord::Migration[6.0]
  def change
    rename_column :occasions, :name, :title
  end
end
