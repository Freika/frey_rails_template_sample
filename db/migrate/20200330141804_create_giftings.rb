# frozen_string_literal: true

class CreateGiftings < ActiveRecord::Migration[6.0]
  def change # rubocop:disable Metrics/MethodLength
    create_table :giftings do |t|
      t.integer :user_id, null: false
      t.integer :gift_id, null: false
      t.integer :occasion_id, null: false
      t.integer :recipient_id, null: false

      t.timestamps
    end
    add_index :giftings, :user_id
    add_index :giftings, :gift_id
    add_index :giftings, :occasion_id
    add_index :giftings, :recipient_id
  end
end
