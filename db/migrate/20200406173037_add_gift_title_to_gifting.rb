# frozen_string_literal: true

class AddGiftTitleToGifting < ActiveRecord::Migration[6.0]
  def change
    add_column :giftings, :gift_title, :string, null: false
  end
end
