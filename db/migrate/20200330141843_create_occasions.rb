# frozen_string_literal: true

class CreateOccasions < ActiveRecord::Migration[6.0]
  def change
    create_table :occasions do |t|
      t.string :name, null: false
      t.date :happens_on

      t.timestamps
    end
    add_index :occasions, :name
  end
end
