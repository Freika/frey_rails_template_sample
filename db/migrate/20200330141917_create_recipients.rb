# frozen_string_literal: true

class CreateRecipients < ActiveRecord::Migration[6.0]
  def change
    create_table :recipients do |t|
      t.string :title, null: false

      t.timestamps
    end
    add_index :recipients, :title
  end
end
