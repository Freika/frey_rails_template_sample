User.create!(email: 'test@test.com', password: '00000000')

Occasion.insert_all(
  [
    { title: 'Новый год' },
    { title: 'Рождество' },
    { title: '23 февраля' },
    { title: '8 марта' },
    { title: 'Пасха' },
    { title: '1 сентября' }
  ]
)

Recipient.insert_all(
  [
    { title: 'Друг' },
    { title: 'Знакомый' },
    { title: 'Коллега' },
    { title: 'Муж' },
    { title: 'Жена' },
    { title: 'Парень' },
    { title: 'Девушка' }
  ]
)
