# == Schema Information
#
# Table name: giftings
#
#  id           :bigint           not null, primary key
#  gift_title   :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  gift_id      :integer          not null
#  occasion_id  :integer          not null
#  recipient_id :integer          not null
#  user_id      :integer          not null
#
# Indexes
#
#  index_giftings_on_gift_id       (gift_id)
#  index_giftings_on_occasion_id   (occasion_id)
#  index_giftings_on_recipient_id  (recipient_id)
#  index_giftings_on_user_id       (user_id)
#
FactoryBot.define do
  factory :gifting do
    user_id { 1 }
    gift_id { 1 }
    occasion_id { 1 }
    recipient_id { 1 }
    gift_title { 'Test gift title' }
  end
end
