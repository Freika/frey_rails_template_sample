# == Schema Information
#
# Table name: recipients
#
#  id         :bigint           not null, primary key
#  enabled    :boolean
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_recipients_on_title  (title)
#
FactoryBot.define do
  factory :recipient do
    title { "MyString" }
  end
end
