# == Schema Information
#
# Table name: occasions
#
#  id         :bigint           not null, primary key
#  enabled    :boolean
#  happens_on :date
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_occasions_on_title  (title)
#
FactoryBot.define do
  factory :occasion do
    title { "MyString" }
    happens_on { "2020-03-30" }
  end
end
