# == Schema Information
#
# Table name: gifts
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_gifts_on_title  (title)
#
FactoryBot.define do
  factory :gift do
    title { "MyString" }
  end
end
