# frozen_string_literal: true

describe Gifts::Update do
  let!(:user)              { create(:user) }
  let!(:occasion)          { create(:occasion) }
  let!(:recipient)         { create(:recipient) }
  let!(:gift)              { create(:gift, title: 'Tesla truck RC') }
  let!(:gifting)           do
    create(
      :gifting,
      user_id: user.id,
      occasion_id: occasion.id,
      recipient_id: recipient.id,
      gift_id: gift.id,
      gift_title: 'test gift'
    )
  end
  let!(:occasion_finder)   { OccasionContainer[:find] }
  let!(:recipient_finder)  { RecipientContainer[:find] }
  let!(:gift_finder)       { GiftContainer[:find] }
  let!(:gifting_finder) { GiftingContainer[:find] }
  let!(:updater_class) do
    described_class.new(
      occasion_finder: occasion_finder,
      recipient_finder: recipient_finder,
      gift_finder: gift_finder,
      gifting_finder: gifting_finder
    )
  end

  context 'when success' do
    let(:gift_params) { { title: 'Parrot Copter', occasion_id: occasion.id, recipient_id: recipient.id } }

    it 'updates a gift' do
      updater_class.call(gift.id, gift_params, user.id)

      expect(gift.reload.title).to eq(gift_params[:title])
    end
  end

  context 'when failure' do
    let(:gift_params) { { title: 'Parrot Copter', occasion_id: 999, recipient_id: recipient.id } }

    it 'failes on occasion' do
      result = updater_class.call(gift.id, gift_params, user.id)

      expect(result).to be_failure
    end
  end
end
