# frozen_string_literal: true

describe Gifts::Create do
  let(:user)        { create(:user) }
  let(:occasion)    { create(:occasion) }
  let(:recipient)   { create(:recipient) }
  let(:occasion_finder) { OccasionContainer[:find] }
  let(:recipient_finder) { RecipientContainer[:find] }
  let(:creator_class) { described_class.new(occasion_finder: occasion_finder, recipient_finder: recipient_finder) }

  context 'when success' do
    let(:gift_params) { { title: 'Parrot Copter', occasion_id: occasion.id, recipient_id: recipient.id } }

    it 'creates a gift' do
      expect { creator_class.call(gift_params, user.id) }.to change(Gift, :count).by(1)
    end

    it 'creates a gifting' do
      result = creator_class.call(gift_params, user.id).value!

      expect(result.occasion).to eq(occasion)
      expect(result.recipient).to eq(recipient)
      expect(result.gift.title).to eq(gift_params[:title])
    end
  end

  context 'when failure' do
    let(:gift_params) { { title: 'Parrot Copter', occasion_id: 999, recipient_id: recipient.id } }

    it 'failes on occasion' do
      result = creator_class.call(gift_params, user.id)

      expect(result).to be_failure
    end
  end
end
