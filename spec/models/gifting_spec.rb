# frozen_string_literal: true

# == Schema Information
#
# Table name: giftings
#
#  id           :bigint           not null, primary key
#  gift_title   :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  gift_id      :integer          not null
#  occasion_id  :integer          not null
#  recipient_id :integer          not null
#  user_id      :integer          not null
#
# Indexes
#
#  index_giftings_on_gift_id       (gift_id)
#  index_giftings_on_occasion_id   (occasion_id)
#  index_giftings_on_recipient_id  (recipient_id)
#  index_giftings_on_user_id       (user_id)
#

describe Gifting, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:recipient) }
  it { is_expected.to belong_to(:occasion) }
  it { is_expected.to belong_to(:gift) }

  it { is_expected.to validate_presence_of(:user_id) }
  it { is_expected.to validate_presence_of(:gift_id) }
  it { is_expected.to validate_presence_of(:occasion_id) }
  it { is_expected.to validate_presence_of(:recipient_id) }
  it { is_expected.to validate_presence_of(:gift_title) }
end
