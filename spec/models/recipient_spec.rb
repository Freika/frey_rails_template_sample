# frozen_string_literal: true

# == Schema Information
#
# Table name: recipients
#
#  id         :bigint           not null, primary key
#  enabled    :boolean
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_recipients_on_title  (title)
#

describe Recipient, type: :model do
  it { is_expected.to have_many(:giftings) }
  it { is_expected.to have_many(:users).through(:giftings) }
  it { is_expected.to have_many(:gifts).through(:giftings) }
  it { is_expected.to have_many(:occasions).through(:giftings) }

  it { is_expected.to validate_presence_of(:title) }
end
