# frozen_string_literal: true

# == Schema Information
#
# Table name: gifts
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_gifts_on_title  (title)
#

describe Gift, type: :model do
  it { is_expected.to have_many(:giftings) }
  it { is_expected.to have_many(:users).through(:giftings) }
  it { is_expected.to have_many(:recipients).through(:giftings) }
  it { is_expected.to have_many(:occasions).through(:giftings) }

  it { is_expected.to validate_presence_of(:title) }
end
