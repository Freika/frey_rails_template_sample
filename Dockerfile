FROM ruby:2.6.5-alpine

ENV RACK_ENV=production
ENV RAILS_ENV=production
ENV RAILS_LOG_TO_STDOUT=1
ENV APP_PATH=/app
ENV PORT=80
ENV ENV=production
ENV BUNDLER_VERSION="2.1.4"
WORKDIR $APP_PATH

RUN apk update && \
    apk upgrade && \
    apk --no-cache add \
    nodejs \
    openrc tzdata bash build-base curl \
    libxml2-dev libxslt-dev postgresql-dev postgresql-client \
    libgcc libstdc++ libx11 glib libxrender libxext libintl \
    libcrypto1.0 libssl1.0 yarn file

RUN gem install bundler
RUN apk --no-cache add openrc tzdata bash

COPY Gemfile* $APP_PATH/

RUN apk --no-cache add --virtual .build-deps g++ make cmake
RUN bundle install --without development test --retry=10 --jobs=10 --quiet
RUN apk del .build-deps

# Make preparations
COPY . $APP_PATH

RUN ASSETS_PRECOMPILATION=1 SECRET_KEY_BASE=1 bundle exec rake assets:precompile

EXPOSE 80
