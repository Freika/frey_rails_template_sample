# frozen_string_literal: true

class Occasions::Find < BaseService
  def call(occasion_id)
    occasion = Occasion.find_by(id: occasion_id)
    occasion ? Success(occasion) : Failure(:occasion_not_found)
  end
end
