# frozen_string_literal: true

class Giftings::Find < BaseService
  def call(gift_id:, user_id:)
    gifting = Gifting.find_by(gift_id: gift_id, user_id: user_id)
    gifting ? Success(gifting) : Failure(:gifting_not_found)
  end
end
