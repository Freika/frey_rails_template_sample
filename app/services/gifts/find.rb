# frozen_string_literal: true

class Gifts::Find < BaseService
  def call(gift_id)
    gift = Gift.find_by(id: gift_id)
    gift ? Success(gift) : Failure(:gift_not_found)
  end
end
