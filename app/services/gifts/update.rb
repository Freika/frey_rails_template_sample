# frozen_string_literal: true

class Gifts::Update < BaseService
  def initialize(occasion_finder:, recipient_finder:, gift_finder:, gifting_finder:)
    @occasion_finder  = occasion_finder
    @recipient_finder = recipient_finder
    @gift_finder      = gift_finder
    @gifting_finder   = gifting_finder
  end

  def call(gift_id, params, current_user_id)
    gift      = yield gift_finder.call(gift_id)
    occasion  = yield occasion_finder.call(params[:occasion_id])
    recipient = yield recipient_finder.call(params[:recipient_id])
    gifting   = yield gifting_finder.call(gift_id: gift.id, user_id: current_user_id)
    update    = yield update(gifting, gift, occasion.id, recipient.id, params[:title])

    update ? Success(update) : Failure(:could_not_create_gift)
  end

  private

  attr_reader :occasion_finder, :recipient_finder, :gift_finder, :gifting_finder

  def update(gifting, gift, occasion_id, recipient_id, gift_title)
    updated = ActiveRecord::Base.transaction do
      gifting.update(occasion_id: occasion_id, recipient_id: recipient_id)
      gift.update(title: gift_title)
    end

    updated ? Success(updated) : Failure(:update_failed)
  end
end
