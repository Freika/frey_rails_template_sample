# frozen_string_literal: true

class Gifts::Create < BaseService
  def initialize(occasion_finder:, recipient_finder:)
    @occasion_finder = occasion_finder
    @recipient_finder = recipient_finder
  end

  def call(params, current_user_id) # rubocop:disable Metrics/MethodLength
    occasion  = yield occasion_finder.call(params[:occasion_id])
    recipient = yield recipient_finder.call(params[:recipient_id])
    gift      = yield create_gift(params[:title])

    gifting = Gifting.create(
      user_id: current_user_id,
      occasion_id: occasion.id,
      recipient_id: recipient.id,
      gift_id: gift.id,
      gift_title: gift.title
    )

    gifting.persisted? ? Success(gifting) : Failure(:could_not_create_gift)
  end

  private

  attr_reader :occasion_finder, :recipient_finder

  def create_gift(title)
    gift = Gift.create(title: title)
    gift ? Success(gift) : Failure(:gift_creating_error)
  end
end
