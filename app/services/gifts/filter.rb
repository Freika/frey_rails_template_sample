# frozen_string_literal: true

class Gifts::Filter < BaseService
  def initialize(giftings:)
    @giftings = giftings
  end

  def call(params)
    @giftings = filter_recipients(params) if params['recipient'].present?
    @giftings = filter_occasions(params)  if params['occasion'].present?
    @giftings
  end

  private

  def filter_recipients(params)
    @giftings.joins(:recipient).merge(Recipient.where(title: params['recipient']))
  end

  def filter_occasions(params)
    @giftings.joins(:occasion).merge(Occasion.where(title: params['occasion']))
  end
end
