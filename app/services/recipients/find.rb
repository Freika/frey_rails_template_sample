# frozen_string_literal: true

class Recipients::Find < BaseService
  def call(recipient_id)
    recipient = Recipient.find_by(id: recipient_id)
    recipient ? Success(recipient) : Failure(:recipient_not_found)
  end
end
