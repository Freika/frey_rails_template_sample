# frozen_string_literal: true

class GiftsController < ApplicationController
  before_action :authenticate_user!

  def index
    @giftings = GiftContainer[:filter].call(params).page(params[:page])

    @occasions = Occasion.pluck(:title)
    @recipients = Recipient.pluck(:title)
  end

  def new
    @gift = Gift.new
    @occasions = Occasion.pluck(:title, :id)
    @recipients = Recipient.pluck(:title, :id)
  end

  def edit
    @gift = current_user.gifts.find(params[:id])
    @occasions = Occasion.pluck(:title, :id)
    @recipients = Recipient.pluck(:title, :id)
  end

  def create
    creating = GiftContainer[:create].call(gift_params, current_user.id)

    if creating.success?
      redirect_to gifts_path, notice: 'Gift created!'
    else
      redirect_to gifts_path, alert: 'Something went wrong'
    end
  end

  def update
    update = GiftContainer[:update].call(params[:id], gift_params, current_user.id)

    if update.success?
      redirect_to my_gifts_path, notice: 'Gift successfully updated'
    else
      redirect_to my_gifts_path, notice: update.failure
    end
  end

  def destroy
    @gift = current_user.gifts.find(params[:id])

    if @gift.destroy
      redirect_to my_gifts_path, notice: 'Gift successfully deleted'
    else
      redirect_to my_gifts_path, alert: 'Something went wrong'
    end
  end

  # User's gifts
  def my
    @giftings =
      Gifting.includes(:gift, :recipient).where(user_id: current_user.id).page(params[:page])
  end

  private

  def gift_params
    params.require(:gift).permit(:title, :occasion_id, :recipient_id)
  end
end
