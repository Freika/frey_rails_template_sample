# frozen_string_literal: true

# == Schema Information
#
# Table name: recipients
#
#  id         :bigint           not null, primary key
#  enabled    :boolean
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_recipients_on_title  (title)
#
class Recipient < ApplicationRecord
  has_many :giftings, dependent: :restrict_with_exception
  has_many :users, through: :giftings
  has_many :gifts, through: :giftings
  has_many :occasions, through: :giftings

  validates :title, presence: true
end
