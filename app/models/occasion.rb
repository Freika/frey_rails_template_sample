# frozen_string_literal: true

# == Schema Information
#
# Table name: occasions
#
#  id         :bigint           not null, primary key
#  enabled    :boolean
#  happens_on :date
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_occasions_on_title  (title)
#
class Occasion < ApplicationRecord
  has_many :giftings, dependent: :restrict_with_exception
  has_many :users, through: :giftings
  has_many :gifts, through: :giftings
  has_many :recipients, through: :giftings

  validates :title, presence: true
end
