# frozen_string_literal: true

# == Schema Information
#
# Table name: gifts
#
#  id         :bigint           not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_gifts_on_title  (title)
#
class Gift < ApplicationRecord
  has_many :giftings, dependent: :destroy
  has_many :users, through: :giftings
  has_many :occasions, through: :giftings
  has_many :recipients, through: :giftings

  validates :title, presence: true
end
