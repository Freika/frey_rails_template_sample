# frozen_string_literal: true

# == Schema Information
#
# Table name: giftings
#
#  id           :bigint           not null, primary key
#  gift_title   :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  gift_id      :integer          not null
#  occasion_id  :integer          not null
#  recipient_id :integer          not null
#  user_id      :integer          not null
#
# Indexes
#
#  index_giftings_on_gift_id       (gift_id)
#  index_giftings_on_occasion_id   (occasion_id)
#  index_giftings_on_recipient_id  (recipient_id)
#  index_giftings_on_user_id       (user_id)
#
class Gifting < ApplicationRecord
  belongs_to :user
  belongs_to :occasion
  belongs_to :recipient
  belongs_to :gift

  validates :user_id, presence: true
  validates :gift_id, presence: true
  validates :occasion_id, presence: true
  validates :recipient_id, presence: true
  validates :gift_title, presence: true
end
