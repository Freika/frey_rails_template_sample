# frozen_string_literal: true

class OccasionContainer
  extend Dry::Container::Mixin

  register(:find) { Occasions::Find.new }
end
