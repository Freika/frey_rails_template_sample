# frozen_string_literal: true

class GiftContainer
  extend Dry::Container::Mixin

  register(:create) do
    Gifts::Create.new(
      occasion_finder: OccasionContainer[:find],
      recipient_finder: RecipientContainer[:find]
    )
  end

  register(:update) do
    Gifts::Update.new(
      occasion_finder: OccasionContainer[:find],
      recipient_finder: RecipientContainer[:find],
      gifting_finder: GiftingContainer[:find],
      gift_finder: self[:find]
    )
  end

  register(:find) { Gifts::Find.new }
  register(:filter) { Gifts::Filter.new(giftings: Gifting.includes(:gift, :recipient)) }

  # register(:zalo_config, memoize: true) { ZaloConfig.new }
end
