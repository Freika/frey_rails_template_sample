# frozen_string_literal: true

class GiftingContainer
  extend Dry::Container::Mixin

  register(:find) { Giftings::Find.new }
end
