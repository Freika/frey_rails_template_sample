# frozen_string_literal: true

class RecipientContainer
  extend Dry::Container::Mixin

  register(:find) { Recipients::Find.new }
end
